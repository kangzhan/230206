package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private OverflowListDao overflowListDao;
    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;
    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr) {
        // 设置当前操作用户
        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        overflowList.setUserId(currentUser.getUserId());
        overflowListDao.saveOverflowList(overflowList);
        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {}.getType());
        for (OverflowListGoods overflowListGoods : overflowListGoodsList){
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            overflowListGoodsDao.saveOverflowListGoods(overflowListGoods);
            // 修改商品库存，状态
            Goods goods = goodsDao.getGoodsByGoodsId(overflowListGoods.getGoodsId());

            goods.setInventoryQuantity(goods.getInventoryQuantity()+overflowListGoods.getGoodsNum());

            goods.setState(2);

            goodsDao.updateGoods(goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getList(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> overflowLists = overflowListDao.getOverflowList(sTime,eTime);
        for (OverflowList overflowList : overflowLists){
            User user = userDao.getUserById(overflowList.getUserId());
            overflowList.setTrueName(user.getTrueName());
        }
        map.put("rows",overflowLists);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoods = overflowListGoodsDao.getOverflowListGoods(overflowListId);
        map.put("rows",overflowListGoods);
        return map;
    }
}
