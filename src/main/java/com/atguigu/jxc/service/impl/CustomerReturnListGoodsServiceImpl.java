package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerReturnListGoodsServiceImpl implements CustomerReturnListGoodsService {
    @Autowired
    private CustomerReturnListGoodsDao customerReturnListGoodsDao;
    @Override
    public Integer getCustomerReturnTotalByGoodsId(Integer goodsId) {
        Integer total = customerReturnListGoodsDao.getCustomerReturnTotalByGoodsId(goodsId);
        return total == null? 0 : total;
    }
}
