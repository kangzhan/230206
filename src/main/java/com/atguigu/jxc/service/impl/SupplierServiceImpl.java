package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;
    @Override
    public Map<String, Object> getList(Integer page, Integer rows, String supplierName) {
        Map<String,Object> map = new HashMap<>();
        if (page == 0) {
            page = 1;
        }
        int offSet = (page - 1) * rows;
        List<Supplier> supplierList = supplierDao.getList(offSet,rows,supplierName);
        map.put("total",supplierDao.getSupplierCounts(supplierName));
        map.put("rows",supplierList);
        return map;
    }

    @Override
    public ServiceVO save(Supplier supplier) {
        if (supplier.getSupplierId() == null) {
            supplierDao.saveSupplier(supplier);
        }else {
            supplierDao.updateSupplier(supplier);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {
        String[] strArr = ids.split(",");
        Integer[] idsArr = new Integer[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            idsArr[i] = Integer.parseInt(strArr[i]);
            supplierDao.delete(idsArr[i]);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
