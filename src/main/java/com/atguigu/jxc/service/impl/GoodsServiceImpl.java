package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private SaleListGoodsService saleListGoodsService;
    @Autowired
    private CustomerReturnListGoodsService customerReturnListGoodsService;
    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String,Object> map = new HashMap<>();
        if (page == 0) {
            page = 1;
        }
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getGoodsInventoryList(offSet,rows,codeOrName,goodsTypeId);
        for (Goods goods : goodsList) {
            goods.setSaleTotal(saleListGoodsService.getSaleTotalByGoodsId(goods.getGoodsId()) - customerReturnListGoodsService.getCustomerReturnTotalByGoodsId(goods.getGoodsId()));

        }
        map.put("rows",goodsList);
        map.put("total",goodsDao.getGoodsInventoryCount(codeOrName,goodsTypeId));
        return map;
    }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        if (page == 0) {
            page = 1;
        }
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getList(offSet,rows,goodsName,goodsTypeId);
        Map<String,Object> map = new HashMap<>();
        map.put("total",goodsDao.getListCounts(goodsName,goodsTypeId));
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public ServiceVO save(Goods goods) {
        if (goods.getGoodsId() == null) {
            goods.setLastPurchasingPrice(goods.getPurchasingPrice());
            goods.setInventoryQuantity(0);
            goods.setState(0);
            goodsDao.saveGoods(goods);
        }else {
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(Integer goodsId) {
        //要判断商品状态,入库、有进货和销售单据的不能删除
        Goods goods = goodsDao.getGoodsByGoodsId(goodsId);
        if (goods.getState() == 1) {

            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);

        } else if (goods.getState() == 2) {

            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);

        } else {
            goodsDao.deleteGoods(goodsId);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object> map = new HashMap<>();
        if (page == 0) {
            page = 1;
        }
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet,rows,nameOrCode);
        map.put("total",goodsList.size());
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object> map = new HashMap<>();
        if (page == 0) {
            page = 1;
        }
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offSet,rows,nameOrCode);
        map.put("total",goodsList.size());
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        Goods goods = new Goods();
        goods.setGoodsId(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);
        goodsDao.updateGoods(goods);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        //要判断商品状态 入库、有进货和销售单据的不能删除
        Goods goods = goodsDao.getGoodsByGoodsId(goodsId);
        if (goods.getState() == 1) {
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);
        } else if (goods.getState() == 2) {
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        } else {
            goods.setInventoryQuantity(0);
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getListAlarm() {
        Map<String,Object> map = new HashMap<>();
        List<Goods> goodsList = goodsDao.getListAlarm();
        map.put("rows",goodsList);
        return map;
    }


}
