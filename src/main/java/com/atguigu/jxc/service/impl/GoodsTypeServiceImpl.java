package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsTypeService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {
    @Autowired
    private GoodsTypeDao goodsTypeDao;
    @Autowired
    private GoodsDao goodsDao;

    @Override
    public String loadGoodsType() {
        JsonArray allGoodsType = getAllGoodsType(-1);
        return allGoodsType.toString();
    }

    @Override
    public ServiceVO save(String goodsTypeName, Integer pId) {
        GoodsType goodsType = new GoodsType(goodsTypeName, 0, pId);
        goodsTypeDao.saveGoodsType(goodsType);
        GoodsType goodsParent = goodsTypeDao.getGoodsTypeById(pId);
        if (goodsParent.getGoodsTypeState() == 0) {
            goodsParent.setGoodsTypeState(1);
            goodsTypeDao.updateGoodsTypeState(goodsParent);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteGoodsType(Integer goodsTypeId) {
        List<Goods> goodsList = goodsDao.getGoodsByTypeId(goodsTypeId);
        if (goodsList.size() != 0) {
            return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE,ErrorCode.GOODS_TYPE_ERROR_MESS);
        }
        GoodsType goodsType = goodsTypeDao.getGoodsTypeById(goodsTypeId);
        List<GoodsType> goodsTypeList = goodsTypeDao.getGoodsTypeByParentId(goodsType.getPId());
        if (goodsTypeList.size() == 1) {
            GoodsType goodsTypeById = goodsTypeDao.getGoodsTypeById(goodsType.getPId());
            goodsTypeById.setGoodsTypeState(0);
            goodsTypeDao.updateGoodsTypeState(goodsTypeById);
        }
        goodsTypeDao.deleteGoodsType(goodsTypeId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    public JsonArray getAllGoodsType(Integer parentId){
        JsonArray jsonArray = getGoodsTypeByParentId(parentId);
        for (int i = 0;i < jsonArray.size();i++){
            JsonObject obj = (JsonObject) jsonArray.get(i);
            if (obj.get("state").getAsString().equals("open")) {
                //是叶子节点
                continue;
            }else {
                obj.add("children",getAllGoodsType(obj.get("id").getAsInt()));
            }
        }
        return jsonArray;
    }

    public JsonArray getGoodsTypeByParentId(Integer parentId){
        JsonArray jsonArray = new JsonArray();
        //根据父id获取所有子商品类别
        List<GoodsType> goodsTypeList = goodsTypeDao.getGoodsTypeByParentId(parentId);
        for (GoodsType goodsType : goodsTypeList){
            JsonObject object = new JsonObject();
            object.addProperty("id",goodsType.getGoodsTypeId());
            object.addProperty("text",goodsType.getGoodsTypeName());
            if (goodsType.getGoodsTypeState() == 1) { //根节点
                object.addProperty("state","closed");
            }else { //叶节点
                object.addProperty("state", "open");
            }
            object.addProperty("iconCls","goods-type");
            JsonObject attributes = new JsonObject();
            attributes.addProperty("state",goodsType.getGoodsTypeState());
            object.add("attributes",attributes);
            jsonArray.add(object);
        }
        return jsonArray;
    }
}
