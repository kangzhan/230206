package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;

    @Override
    public Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName) {
        Map<String,Object> map = new HashMap<>();
        if (page == 0) {
            page = 1;
        }
        int offSet = (page - 1) * rows;
        List<Customer> customerList = customerDao.getCustomerList(offSet,rows,customerName);
        map.put("rows",customerList);
        map.put("total",customerDao.getCustomerCount(customerName));
        return map;
    }

    @Override
    public ServiceVO save(Customer customer) {
        if (customer.getCustomerId() == null) {
            customerDao.saveCustomer(customer);
        }else {
            customerDao.updateCustomer(customer);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteCustomer(String ids) {
        String[] idsArr = ids.split(",");
        for (int i=0 ; i<idsArr.length ; i++){
            customerDao.deleteCustomer(Integer.parseInt(idsArr[i]));
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }


}
