package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private DamageListDao damageListDao;
    @Autowired
    private DamageListGoodsDao damageListGoodsDao;
    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {
        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {}.getType());
        // 设置当前操作用户
        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        damageList.setUserId(currentUser.getUserId());

        damageListDao.saveDamageList(damageList);

        for (DamageListGoods damageListGoods : damageListGoodsList){
            damageListGoods.setDamageListId(damageList.getDamageListId());
            damageListGoodsDao.savedamageListGoods(damageListGoods);
            // 修改商品库存，状态
            Goods goods = goodsDao.getGoodsByGoodsId(damageListGoods.getGoodsId());
            goods.setInventoryQuantity(goods.getInventoryQuantity() - damageListGoods.getGoodsNum());
            goods.setState(2);
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getList(String sTime, String eTime) {
        Map<String,Object> map = new HashMap<>();
        List<DamageList> damageLists = damageListDao.getDamageList(sTime,eTime);
        for (DamageList damageList : damageLists){
            User user = userDao.getUserById(damageList.getUserId());
            damageList.setTrueName(user.getTrueName());
        }
        map.put("rows",damageLists);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer damageListId) {
        Map<String,Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.getDamageListGoods(damageListId);
        map.put("rows",damageListGoodsList);
        return map;
    }
}
