package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

public interface GoodsTypeService {

    String loadGoodsType();

    ServiceVO save(String goodsTypeName, Integer pId);

    ServiceVO deleteGoodsType(Integer goodsTypeId);
}
