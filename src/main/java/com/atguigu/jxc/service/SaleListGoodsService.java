package com.atguigu.jxc.service;

public interface SaleListGoodsService {
    Integer getSaleTotalByGoodsId(Integer goodsId);
}
