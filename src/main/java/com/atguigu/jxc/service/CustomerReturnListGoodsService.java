package com.atguigu.jxc.service;


public interface CustomerReturnListGoodsService {
    Integer getCustomerReturnTotalByGoodsId(Integer goodsId);
}
