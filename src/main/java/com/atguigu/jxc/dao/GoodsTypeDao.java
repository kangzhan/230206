package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsTypeDao {
    List<GoodsType> getGoodsTypeByParentId(@Param("parentId") Integer parentId);

    void saveGoodsType(GoodsType goodsType);

    GoodsType getGoodsTypeById(@Param("pId") Integer pId);

    void updateGoodsTypeState(GoodsType goodsParent);

    void deleteGoodsType(@Param("goodsTypeId") Integer goodsTypeId);
}
