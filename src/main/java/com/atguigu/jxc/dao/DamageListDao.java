package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DamageListDao {
    void saveDamageList(DamageList damageList);

    List<DamageList> getDamageList(@Param("sTime") String sTime,@Param("eTime") String eTime);
}
