package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsDao {
    List<Goods> getGoodsInventoryList(@Param("offSet") int offSet,@Param("rows") Integer rows,@Param("codeOrName") String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);

    Integer getGoodsInventoryCount(@Param("codeOrName") String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> getList(@Param("offSet") int offSet,@Param("rows") Integer rows,@Param("goodsName") String goodsName,@Param("goodsTypeId") Integer goodsTypeId);

    Integer getListCounts(String goodsName, Integer goodsTypeId);

    List<Goods> getGoodsByTypeId(Integer goodsTypeId);

    void saveGoods(Goods goods);

    void updateGoods(Goods goods);

    Goods getGoodsByGoodsId(Integer goodsId);

    void deleteGoods(Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("offSet") int offSet,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("offSet") int offSet,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    void deleteStock(Integer goodsId);

    List<Goods> getListAlarm();
}
