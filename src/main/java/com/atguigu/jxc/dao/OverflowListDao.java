package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OverflowListDao {
    void saveOverflowList(OverflowList overflowList);

    List<OverflowList> getOverflowList(@Param("sTime") String sTime,@Param("eTime") String eTime);
}
