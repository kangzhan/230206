package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DamageListGoodsDao {
    void savedamageListGoods(DamageListGoods damageListGoods);

    List<DamageListGoods> getDamageListGoods(Integer damageListId);
}
