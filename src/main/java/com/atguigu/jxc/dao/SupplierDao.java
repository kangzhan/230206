package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierDao {

    List<Supplier> getList(@Param("offSet") int offSet,@Param("rows") Integer rows,String supplierName);

    Integer getSupplierCounts(@Param("supplierName") String supplierName);

    void saveSupplier(Supplier supplier);

    void updateSupplier(Supplier supplier);

    void delete(@Param("supplierId") Integer supplierId);
}
