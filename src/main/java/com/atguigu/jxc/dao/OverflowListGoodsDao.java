package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OverflowListGoodsDao {
    void saveOverflowListGoods(OverflowListGoods overflowListGoods);

    List<OverflowListGoods> getOverflowListGoods(Integer overflowListId);
}
