package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerDao {
    List<Customer> getCustomerList(@Param("offSet") int offSet,@Param("rows") Integer rows,@Param("customerName") String customerName);
//
    Integer getCustomerCount(@Param("customerName") String customerName);
//
    void saveCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomer(@Param("customerId") Integer customerId);
}
