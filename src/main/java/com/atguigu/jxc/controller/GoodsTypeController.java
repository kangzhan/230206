package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/goodsType")
public class GoodsTypeController {
    @Autowired
    private GoodsTypeService goodsTypeService;

    @PostMapping(value = "loadGoodsType")
    public String loadGoodsType(){
        return goodsTypeService.loadGoodsType();
    }

    @PostMapping(value = "/save")
    public ServiceVO save(String goodsTypeName,Integer pId){
        return goodsTypeService.save(goodsTypeName,pId);
    }

    @PostMapping(value = "/delete")
    public ServiceVO delete(Integer goodsTypeId){
        return goodsTypeService.deleteGoodsType(goodsTypeId);
    }
}
