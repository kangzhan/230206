package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/damageListGoods")
public class DamageListGoodsController {
    @Autowired
    private DamageListGoodsService damageListGoodsService;

    @PostMapping(value = "/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr){
        return damageListGoodsService.save(damageList,damageListGoodsStr);
    }

    @PostMapping(value = "/list")
    public Map<String,Object> list(String  sTime, String  eTime){
        return damageListGoodsService.getList(sTime,eTime);
    }

    @PostMapping(value = "/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        return damageListGoodsService.getGoodsList(damageListId);
    }
}
