package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(tags = "库存管理")
@RestController
@RequestMapping("/goods")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @ApiOperation(value = "当前库存查询")
    @PostMapping(value = "/listInventory")
    public Map<String,Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId){
        Map<String, Object> map = goodsService.listInventory(page,rows,codeOrName,goodsTypeId);
        return map;
    }

    @PostMapping(value = "/list")
    public Map<String,Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId){
        return goodsService.list(page,rows,goodsName,goodsTypeId);
    }

    @PostMapping(value = "/save")
    public ServiceVO save(Goods goods){
        return goodsService.save(goods);
    }

    @PostMapping(value = "/delete")
    public ServiceVO delete(Integer goodsId){
        return goodsService.delete(goodsId);
    }

    @PostMapping(value = "/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        return goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
    }

    @PostMapping(value = "/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        return goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
    }

    @PostMapping(value = "/saveStock")
    public ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,double purchasingPrice){
        return goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
    }

    @PostMapping(value = "/deleteStock")
    public ServiceVO deleteStock(Integer goodsId){
        return goodsService.deleteStock(goodsId);
    }

    @PostMapping(value = "/listAlarm")
    public Map<String,Object> getListAlarm(){
        return goodsService.getListAlarm();
    }
}
