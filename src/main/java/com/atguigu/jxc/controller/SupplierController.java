package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    @PostMapping(value = "/list")
    public Map<String,Object> list(Integer page, Integer rows,String supplierName){
        return supplierService.getList(page,rows,supplierName);
    }

    @PostMapping(value = "/save")
    public ServiceVO saveSupplier(Supplier supplier){
        return supplierService.save(supplier);
    }

    @PostMapping(value = "/delete")
    public ServiceVO deleteSupplier(String ids){
        return supplierService.delete(ids);
    }
}
