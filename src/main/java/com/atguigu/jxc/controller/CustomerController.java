package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping(value = "/list")
    public Map<String,Object> list(Integer page, Integer rows, String customerName){
        return customerService.getCustomerList(page,rows,customerName);
    }

    @PostMapping(value = "/save")
    public ServiceVO save(Customer customer){
        return customerService.save(customer);
    }

    @PostMapping(value = "/delete")
    public ServiceVO delete(String ids){
        return customerService.deleteCustomer(ids);
    }
}
